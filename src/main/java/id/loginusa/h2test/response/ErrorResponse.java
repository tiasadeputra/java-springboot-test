package id.loginusa.h2test.response;

import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;


public class ErrorResponse {
    private String status;
    private int code;
    private List<String> message;

    public ErrorResponse(String status, int code, List<String> message) {
        this.status = status;
        this.code = code;
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public int getCode() {
        return code;
    }

    public List<String> getMessage() {
        return message;
    }

    public static ErrorResponse notFound(Long resourceId) {
        List<String> message = new ArrayList<>();
        message.add(HttpStatus.NOT_FOUND.name());
        message.add("Cannot find resource with id " + resourceId);
        return new ErrorResponse(HttpStatus.NOT_FOUND.name(), 30000, message);
    }

    public static ErrorResponse conflict(String[] uniqueValue) {
        List<String> message = new ArrayList<>();
        message.add(HttpStatus.CONFLICT.name());
        message.add("Record with unique value " + uniqueValue + " already exists in the system");
        return new ErrorResponse(HttpStatus.CONFLICT.name(), 30001, message);
    }

    public static ErrorResponse invalidField(String fieldName, String rejectedValue) {
        List<String> message = new ArrayList<>();
        message.add(HttpStatus.UNPROCESSABLE_ENTITY.name());
        message.add("Invalid value for field " + fieldName + ", rejected value: " + rejectedValue);
        return new ErrorResponse(HttpStatus.UNPROCESSABLE_ENTITY.name(), 30002, message);
    }

    public static ErrorResponse systemError(List<String> message) {
        return new ErrorResponse(HttpStatus.INTERNAL_SERVER_ERROR.name(), 80000, message);
    }
}
