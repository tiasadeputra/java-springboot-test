package id.loginusa.h2test.controller;

import id.loginusa.h2test.entity.User;
import id.loginusa.h2test.entity.UserSettingKeyLOV;
import id.loginusa.h2test.payload.UserResponse;
import id.loginusa.h2test.repository.UserSettingKeyLovRepository;
import id.loginusa.h2test.repository.UserSettingRepository;
import id.loginusa.h2test.repository.UsersRepository;
import id.loginusa.h2test.response.ErrorResponse;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PositiveOrZero;
import jakarta.validation.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.*;

@RestController
@RequestMapping("/v1/users")
public class UserController {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private UserSettingRepository userSettingRepository;
    @Autowired
    private UserSettingKeyLovRepository userSettingKeyLovRepository;

    @GetMapping
    public ResponseEntity<Object> getUsers(
            @PositiveOrZero(message = "Offset must be positive or zero")
            @RequestParam(value = "offset", defaultValue = "0") int offset,
            @Min(value = 1, message = "Max Records must be greater than or equal to 1")
            @RequestParam(value = "max_records", defaultValue = "5")int maxRecords) {
        try {
            Pageable paging = PageRequest.of(offset, maxRecords);
            Page<User> userPage = usersRepository.findAllByIsActiveTrue(paging);

            List<User> users = userPage.getContent();
            if (users.isEmpty()) {
                return ResponseEntity.noContent().build();
            }

            Map<String, Object> response = new HashMap<>();
            response.put("user_data", users);
            response.put("max_records", maxRecords);
            response.put("offset", offset);

            return ResponseEntity.ok(response);
        } catch (Exception e) {
            List<String> errorValue = new ArrayList<>();
            errorValue.add(HttpStatus.INTERNAL_SERVER_ERROR.name());
            errorValue.add(e.getMessage());
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ErrorResponse.systemError(errorValue));
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<?> getUserById(@PathVariable("id") @NotNull(message = "ID cannot be null") Long id) {
        // Check if the user with the given ID exists and is active
        Optional<User> userOptional = usersRepository.findById(id);
        if (!userOptional.isPresent()) {
            return new ResponseEntity<>(ErrorResponse.notFound(id), HttpStatus.NOT_FOUND);
        }

        User user = userOptional.get();
        List<UserSettingKeyLOV> userSettings = userSettingKeyLovRepository.findByUser(user);

        // Build the response object
        Map<String, Object> response = new HashMap<>();
        response.put("user_data", userOptional);
        response.put("user_settings", userSettings);

        return ResponseEntity.ok(response);
    }

    @PostMapping
    public ResponseEntity<Object> createUser(@Valid @RequestBody User request) {
        try {

            // Create new user data
            User user = new User();
            user.setSsn(request.getSsn());
            user.setFirstName(request.getFirstName());
            user.setLastName(request.getLastName());
            user.setFamilyName(request.getFamilyName());
            user.setBirthDate(request.getBirthDate());
            user.setCreatedTime(LocalDateTime.now());
            user.setUpdatedTime(LocalDateTime.now());
            user.setCreatedBy("SYSTEM");
            user.setUpdatedBy("SYSTEM");
            user.setActive(true);
            user = usersRepository.save(user);
            System.out.println("SUKSES INSERT "+user.getId());

            // Create new user settings
            List<UserSettingKeyLOV> userSettingKeyLOVS = new ArrayList<>();
            UserSettingKeyLOV newData = new UserSettingKeyLOV();
            newData.setBiometricLogin(false);
            newData.setPushNotification(false);
            newData.setSmsNotification(false);
            newData.setShowOnboarding(false);
            newData.setWidgetOrder("1,2,3,4,5");
            newData.setUser(user);
            UserSettingKeyLOV savedUserSettings = userSettingKeyLovRepository.save(newData);
            userSettingKeyLOVS.add(savedUserSettings);


            // Return response
            UserResponse response = new UserResponse(user, userSettingKeyLOVS);
            return ResponseEntity.status(HttpStatus.CREATED).body(response);
        } catch (Exception e) {
            ConstraintViolationException cve = (ConstraintViolationException) e;
            List<String> errorValue = new ArrayList<>();
            errorValue.add(HttpStatus.INTERNAL_SERVER_ERROR.name());
//            errorValue.add(e.getMessage());

            for (ConstraintViolation cv : cve.getConstraintViolations()) {
                errorValue.add(cv.getMessageTemplate().toString());
            }

            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(ErrorResponse.systemError(errorValue));
        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserResponse> updateUser(
            @PathVariable @NotNull(message = "ID cannot be null") Long id,
            @Valid @RequestBody User request
    ) {

        Optional<User> optionalUser = usersRepository.findById(id);
        if (!optionalUser.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        User user = optionalUser.get();
        // Update user data based on the request
        if (request.getFirstName() != null) {
            user.setFirstName(request.getFirstName());
        }
        if (request.getMiddleName() != null) {
            user.setMiddleName(request.getMiddleName());
        }
        if (request.getFirstName() != null) {
            user.setLastName(request.getLastName());
        }
        if (request.getBirthDate() != null) {
            user.setBirthDate(request.getBirthDate());
        }

        usersRepository.save(user);

        List<UserSettingKeyLOV> userSettings = userSettingKeyLovRepository.findByUser(user);

        UserResponse userResponse = new UserResponse(user, userSettings);
        return ResponseEntity.ok(userResponse);
    }

    @PutMapping("/{id}/settings")
    public ResponseEntity<?> updateUserSettings(
            @PathVariable Long id,
            @Valid @RequestBody List<UserSettingKeyLOV> request
    ) {
        Optional<User> optionalUser = usersRepository.findById(id);
        if (!optionalUser.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        User user = optionalUser.get();

        List<UserSettingKeyLOV> userSettingKeyLOVS = new ArrayList<>();
        for (UserSettingKeyLOV userSettingKeyLOV : request  ) {
            UserSettingKeyLOV newData = new UserSettingKeyLOV();
            newData.setBiometricLogin(userSettingKeyLOV.getBiometricLogin());
            newData.setPushNotification(userSettingKeyLOV.getPushNotification());
            newData.setSmsNotification(userSettingKeyLOV.getSmsNotification());
            newData.setShowOnboarding(userSettingKeyLOV.getShowOnboarding());
            newData.setWidgetOrder(userSettingKeyLOV.getWidgetOrder());
            newData.setUser(user);
            UserSettingKeyLOV savedUserSettings = userSettingKeyLovRepository.save(newData);
            userSettingKeyLOVS.add(savedUserSettings);

        }
        UserResponse userResponse = new UserResponse(user, userSettingKeyLOVS);


        return ResponseEntity.ok(userResponse);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteUser(@PathVariable @NotNull(message = "ID cannot be null") Long id) {

        User userData = usersRepository.findByIdAndIsActiveIsTrue(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));

        userData.setActive(false);
        userData.setDeletedTime(LocalDateTime.now());

        usersRepository.save(userData);
    }

    @PutMapping("/{id}/refresh")
    public ResponseEntity<UserResponse> reactivateUser(@PathVariable @NotNull(message = "ID cannot be null") Long id) {
        Optional<User> optionalUser = usersRepository.findByIdAndIsActiveIsFalse(id);
        if (optionalUser.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        User user = optionalUser.get();
        user.setActive(true);
        user.setDeletedTime(null);
        User savedUser = usersRepository.save(user);

        // Create new user settings
        List<UserSettingKeyLOV> userSettingKeyLOVS = new ArrayList<>();
        UserSettingKeyLOV newData = new UserSettingKeyLOV();
        newData.setBiometricLogin(false);
        newData.setPushNotification(true);
        newData.setSmsNotification(false);
        newData.setShowOnboarding(false);
        newData.setWidgetOrder("1,2,3,4,5");
        newData.setUser(user);
        UserSettingKeyLOV savedUserSettings = userSettingKeyLovRepository.save(newData);
        userSettingKeyLOVS.add(savedUserSettings);


        // Return response
        UserResponse response = new UserResponse(savedUser, userSettingKeyLOVS);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);

    }



}
