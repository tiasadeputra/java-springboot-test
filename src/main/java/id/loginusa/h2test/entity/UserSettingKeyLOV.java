package id.loginusa.h2test.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@Setter
@Getter
@Entity
@Table(name = "user_setting_key_lov")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserSettingKeyLOV {
//    private boolean biometric_login;
//    private boolean push_notification;
//    private boolean sms_notification;
//    private boolean show_onboarding;
//    private String widget_order;
@Id
@GeneratedValue(strategy = GenerationType.IDENTITY)
private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name = "biometric_login", nullable = false)
    private Boolean biometricLogin = false;

    @Column(name = "push_notification", nullable = false)
    private Boolean pushNotification = false;

    @Column(name = "sms_notification", nullable = false)
    private Boolean smsNotification = false;

    @Column(name = "show_onboarding", nullable = false)
    private Boolean showOnboarding = false;

    @Column(name = "widget_order", nullable = false)
    private String widgetOrder = "1,2,3,4,5";

    @Column(name = "created_time", nullable = false)
    private LocalDateTime createdTime = LocalDateTime.now();

    @Column(name = "updated_time", nullable = false)
    private LocalDateTime updatedTime = LocalDateTime.now();

}
