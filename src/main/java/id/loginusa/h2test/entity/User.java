package id.loginusa.h2test.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.annotation.Nonnull;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.validator.constraints.NotBlank;


import java.time.LocalDateTime;


@Getter
@Setter
@Entity
@Data
@Table(name = "users")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class User  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Nonnull
    @Column(name = "id", nullable = false)
    private Long id;

    @NotBlank(message = "SSN is mandatory")
    @Size(min = 16, max = 16, message = "SSN must be exactly 16 characters long.")
    @Column(name = "ssn", nullable = false, unique = true, length = 16)
    private String ssn;

    @NotBlank(message = "First name is mandatory")
    @Size(min = 3, max = 100, message = "First name must be between 3 and 100 characters long.")
    @Column(name = "first_name", nullable = false, length = 100)
    private String firstName;

    @Size(min = 3, max = 100, message = "Middle name must be between 3 and 100 characters long.")
    @Column(name = "middle_name", length = 100, nullable = true)
    private String middleName;

    @Column(name = "last_name", length = 100)
    private String lastName;

    @NotBlank(message = "Family name is mandatory")
    @Size(min = 3, max = 100, message = "Family name must be between 3 and 100 characters long.")
    @Column(name = "family_name", nullable = false, length = 100)
    private String familyName;

    @Column(name = "birth_date", nullable = false)
    private LocalDateTime birthDate;

    @Column(name = "created_time", nullable = false, updatable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private LocalDateTime createdTime;

    @Column(name = "updated_time", columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    private LocalDateTime updatedTime;

    @Size(max = 100, message = "Created by must be less than or equal to 100 characters long.")
    @Column(name = "created_by", nullable = false, length = 100, columnDefinition = "VARCHAR(100) DEFAULT 'SYSTEM'")
    private String createdBy;

    @Size(max = 100, message = "Updated by must be less than or equal to 100 characters long.")
    @Column(name = "updated_by", nullable = false, length = 100, columnDefinition = "VARCHAR(100) DEFAULT 'SYSTEM'")
    private String updatedBy;

    @Column(name = "is_active", nullable = false, columnDefinition = "BOOLEAN DEFAULT TRUE")
    private boolean isActive;

    @Column(name = "deleted_time")
    private LocalDateTime deletedTime;

    public void setSsn(String ssn) {
        if (ssn.length() < 16) {
            ssn = String.format("%0" + (16 - ssn.length()) + "d%s", 0, ssn);
        }
        this.ssn = ssn;
    }
}
