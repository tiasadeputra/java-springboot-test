package id.loginusa.h2test.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import jakarta.annotation.Nonnull;
import jakarta.persistence.*;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
@Entity
@Table(name = "user_setting")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserSetting {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Nonnull
    @Column(name = "id", unique = true)
    private Long Id;

    @Size(min = 3, max = 100, message = "Family name must be between 3 and 100 characters long.")
    @Column(name = "[key]", length = 100, nullable = false)
    private String key;

    @Size(min = 3, max = 100, message = "Family name must be between 3 and 100 characters long.")
    @Column(name = "[value]", length = 100, nullable = false)
    private String value;

    @Column(name = "user_id", nullable = false)
    private String userId;
}
