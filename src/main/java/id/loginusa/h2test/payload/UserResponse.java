package id.loginusa.h2test.payload;

import id.loginusa.h2test.entity.User;
import id.loginusa.h2test.entity.UserSetting;
import id.loginusa.h2test.entity.UserSettingKeyLOV;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserResponse {
    private User user;
    private List<UserSettingKeyLOV> userSettings;

    public UserResponse(User user, List<UserSettingKeyLOV> userSettings) {
        this.user = user;
        this.userSettings = userSettings;
    }

//    public User getUserData() {
//        return user;
//    }

    public List<UserSettingKeyLOV> getUserSettings() {
        return userSettings;
    }
}
