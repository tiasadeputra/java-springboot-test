package id.loginusa.h2test.payload;

//import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

//import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class UserDataRequest {
    private String ssn;
    private String first_name;
    private String middle_name;
    private String last_name;
//    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDateTime birth_date;
}
