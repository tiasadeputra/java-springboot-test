package id.loginusa.h2test.repository;

import id.loginusa.h2test.entity.UserSetting;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSettingRepository extends CrudRepository<UserSetting,Long> {
//    Page<UserSetting> findAllByIsActiveTrue(Pageable pageable);
}
