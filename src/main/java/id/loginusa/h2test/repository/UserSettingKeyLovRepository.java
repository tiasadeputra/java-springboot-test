package id.loginusa.h2test.repository;

import id.loginusa.h2test.entity.User;
import id.loginusa.h2test.entity.UserSettingKeyLOV;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserSettingKeyLovRepository extends CrudRepository<UserSettingKeyLOV, Long> {
    List<UserSettingKeyLOV> findByUser(User user);
}
