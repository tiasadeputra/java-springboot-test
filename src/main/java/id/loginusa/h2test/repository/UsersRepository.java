package id.loginusa.h2test.repository;

import id.loginusa.h2test.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UsersRepository extends CrudRepository<User, Long> {
    Page<User> findAllByIsActiveTrue(Pageable pageable);

    Optional<User> findByIdAndIsActiveIsTrue(Long id);
    Optional<User> findByIdAndIsActiveIsFalse(Long id);
}
